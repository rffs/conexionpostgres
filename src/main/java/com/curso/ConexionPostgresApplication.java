package com.curso;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@EnableAutoConfiguration
@EntityScan(basePackages = { "com.curso.model", "com.curso.modules.*.dto" })
@ComponentScan(basePackages = { "com.curso.config", "com.curso.modules.*.repository",
		"com.curso.modules.*.controller" })
public class ConexionPostgresApplication {

	public static void main(String[] args) {
		SpringApplication.run(ConexionPostgresApplication.class, args);
	}
}
