package com.curso.modules.personas.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.curso.model.LibroAutor;
import com.curso.modules.personas.repository.LibroAutorRepository;

@RestController
@RequestMapping(value = "libroAutor")
public class LibroAutorController {

	@Autowired
	private LibroAutorRepository libroAutorRepository;

	@RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = "application/json")
	public Optional<LibroAutor> getLibroAutor(@PathVariable(value = "id") int id) {
		return this.libroAutorRepository.findById(id);
	}

	@RequestMapping(method = RequestMethod.GET, produces = "application/json")
	public List<LibroAutor> getLibrosAutors() {
		return this.libroAutorRepository.findAll();
	}

	@RequestMapping(method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public void save(@RequestBody LibroAutor libroAutor) {
		this.libroAutorRepository.save(libroAutor);
	}

	@RequestMapping(method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public void update(@RequestBody LibroAutor libroAutor) {
		this.libroAutorRepository.save(libroAutor);
	}

}
