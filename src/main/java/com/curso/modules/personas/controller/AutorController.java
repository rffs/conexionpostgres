package com.curso.modules.personas.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.curso.model.Autor;
import com.curso.modules.personas.repository.AutorRepository;

@RestController
@RequestMapping(value = "autor")
public class AutorController {

	@Autowired
	private AutorRepository autorRepository;

	@RequestMapping(value = "/id/{id}/nombre/{nombre}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public Optional<Autor> getAutor(@PathVariable(value = "id") int id, @PathVariable(value = "nombre") String nombre) {
		return this.autorRepository.findByIdAndNombre(id, nombre);
	}

	@RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Autor> getAutors() {
		return this.autorRepository.findAll();
	}

	@RequestMapping(method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public void save(@RequestBody Autor autor) {
		this.autorRepository.save(autor);
	}

	@RequestMapping(method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public void update(@RequestBody Autor autor) {
		this.autorRepository.save(autor);
	}

	@RequestMapping(method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public void delete(@RequestBody Autor autor) {
		this.autorRepository.delete(autor);
	}

}
