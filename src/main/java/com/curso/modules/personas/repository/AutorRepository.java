package com.curso.modules.personas.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.curso.model.Autor;

@Repository
public interface AutorRepository extends JpaRepository<Autor, Integer> {

	@Query("FROM Autor WHERE id = :id ")
	public Autor buscarPorId(@Param("id") Integer id);

	public Optional<Autor> findByIdAndNombre(Integer id, String nombre);

	public List<Autor> findAll();
}
