package com.curso.modules.personas.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.curso.model.LibroAutor;

@Repository
public interface LibroAutorRepository extends JpaRepository<LibroAutor, Integer> {
	public Optional<LibroAutor> findById(Integer id);

	public List<LibroAutor> findAll();
}
