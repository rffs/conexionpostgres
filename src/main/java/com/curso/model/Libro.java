package com.curso.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the libros database table.
 * 
 */
@Entity
@Table(name="libros")
@NamedQuery(name="Libro.findAll", query="SELECT l FROM Libro l")
public class Libro implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;

	private String editorial;

	private String titulo;

	//bi-directional many-to-one association to LibroAutor
	@OneToMany(mappedBy="libro")
	private List<LibroAutor> librosAutores;

	//bi-directional many-to-one association to Prestamo
	@OneToMany(mappedBy="libro")
	private List<Prestamo> prestamos;

	public Libro() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getEditorial() {
		return this.editorial;
	}

	public void setEditorial(String editorial) {
		this.editorial = editorial;
	}

	public String getTitulo() {
		return this.titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public List<LibroAutor> getLibrosAutores() {
		return this.librosAutores;
	}

	public void setLibrosAutores(List<LibroAutor> librosAutores) {
		this.librosAutores = librosAutores;
	}

	public LibroAutor addLibrosAutore(LibroAutor librosAutore) {
		getLibrosAutores().add(librosAutore);
		librosAutore.setLibro(this);

		return librosAutore;
	}

	public LibroAutor removeLibrosAutore(LibroAutor librosAutore) {
		getLibrosAutores().remove(librosAutore);
		librosAutore.setLibro(null);

		return librosAutore;
	}

	public List<Prestamo> getPrestamos() {
		return this.prestamos;
	}

	public void setPrestamos(List<Prestamo> prestamos) {
		this.prestamos = prestamos;
	}

	public Prestamo addPrestamo(Prestamo prestamo) {
		getPrestamos().add(prestamo);
		prestamo.setLibro(this);

		return prestamo;
	}

	public Prestamo removePrestamo(Prestamo prestamo) {
		getPrestamos().remove(prestamo);
		prestamo.setLibro(null);

		return prestamo;
	}

}