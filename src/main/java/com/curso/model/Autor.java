package com.curso.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the autores database table.
 * 
 */
@Entity
@Table(name="autores")
@NamedQuery(name="Autor.findAll", query="SELECT a FROM Autor a")
public class Autor implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;

	private String nombre;

	//bi-directional many-to-one association to LibroAutor
	@OneToMany(mappedBy="autore")
	private List<LibroAutor> librosAutores;

	public Autor() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public List<LibroAutor> getLibrosAutores() {
		return this.librosAutores;
	}

	public void setLibrosAutores(List<LibroAutor> librosAutores) {
		this.librosAutores = librosAutores;
	}

	public LibroAutor addLibrosAutore(LibroAutor librosAutore) {
		getLibrosAutores().add(librosAutore);
		librosAutore.setAutore(this);

		return librosAutore;
	}

	public LibroAutor removeLibrosAutore(LibroAutor librosAutore) {
		getLibrosAutores().remove(librosAutore);
		librosAutore.setAutore(null);

		return librosAutore;
	}

}