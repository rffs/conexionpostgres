package com.curso.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


/**
 * The persistent class for the libros_autores database table.
 * 
 */
@Entity
@Table(name="libros_autores")
@NamedQuery(name="LibroAutor.findAll", query="SELECT l FROM LibroAutor l")
public class LibroAutor implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;

	//bi-directional many-to-one association to Autor
	@ManyToOne
	@JoinColumn(name="id_autores")
	@JsonIgnoreProperties(value= {"librosAutores"})
	private Autor autore;

	//bi-directional many-to-one association to Libro
	@ManyToOne
	@JoinColumn(name="id_libros")
	@JsonIgnoreProperties(value= {"librosAutores"})
	private Libro libro;

	public LibroAutor() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Autor getAutore() {
		return this.autore;
	}

	public void setAutore(Autor autore) {
		this.autore = autore;
	}

	public Libro getLibro() {
		return this.libro;
	}

	public void setLibro(Libro libro) {
		this.libro = libro;
	}

}